package main

import (
	"fmt"
	"log"
	"os"
	pb "vessel-service/proto/vessel"

	micro "github.com/micro/go-micro"
)

const (
	DEFAULT_MGO_ADDR = "datastore:27017"
)

func main() {

	dbAddr := os.Getenv("DB_HOST")
	if dbAddr == "" {
		dbAddr = DEFAULT_MGO_ADDR
	}

	srv := micro.NewService(micro.Name("vessel"))
	srv.Init()

	session, err := CreateSession(dbAddr)

	if err != nil {
		log.Fatalf("create session error: %v\n", err)
	}

	pb.RegisterVesselServiceHandler(srv.Server(), &handler{session})

	if err := srv.Run(); err != nil {
		fmt.Println(err)
	}

}
