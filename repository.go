package main

import (
	"errors"
	pb "vessel-service/proto/vessel"

	"gopkg.in/mgo.v2"
)

const (
	DB_NAME        = "shippy"
	CON_COLLECTION = "vessels"
)

type Repository interface {
	FindAvailable(*pb.Specification) (*pb.Vessel, error)
	Create(*pb.Vessel) error
}

type VesselRepository struct {
	session *mgo.Session
}

// FindAvailable ...
func (r *VesselRepository) FindAvailable(spec *pb.Specification) (*pb.Vessel, error) {
	var vessels []*pb.Vessel
	r.collection().Find(nil).All(&vessels)

	for _, vessel := range vessels {
		if spec.Capacity <= vessel.Capacity && spec.MaxWeight <= vessel.MaxWeight {
			return vessel, nil
		}
	}
	return nil, errors.New("No vessel found by that spec")
}

// Create ...
func (r *VesselRepository) Create(v *pb.Vessel) error {
	return r.collection().Insert(v)
}

// collection ...
func (repo *VesselRepository) collection() *mgo.Collection {
	return repo.session.DB(DB_NAME).C(CON_COLLECTION)
}
