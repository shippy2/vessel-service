build:
	protoc -I proto proto/vessel/vessel.proto --micro_out=proto --go_out=proto

docker:
	docker build -t vessel-service .

run:
	docker run -p 50052:50051 \
	-e MICRO_SERVER_ADDRESS=:50051 \
	-e MICRO_REGISTRY=mdns \
	vessel-service
