package main

import (
	"context"
	pb "vessel-service/proto/vessel"

	"gopkg.in/mgo.v2"
)

type handler struct {
	session *mgo.Session
}

// FindAvailable ...
func (h *handler) FindAvailable(ctx context.Context, req *pb.Specification, resp *pb.Response) error {
	v, err := h.GetRepo().FindAvailable(req)
	if err != nil {
		return err
	}

	resp.Vessel = v
	return nil
}

// Create ...
func (h *handler) Create(ctx context.Context, req *pb.Vessel, resp *pb.Response) error {
	if err := h.GetRepo().Create(req); err != nil {
		return err
	}

	resp.Vessel = req
	resp.Created = true
	return nil
}

func (h *handler) GetRepo() Repository {
	return &VesselRepository{h.session.Clone()}
}
