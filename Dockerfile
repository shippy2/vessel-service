FROM golang:alpine as builder
RUN apk --no-cache add git
WORKDIR /app
COPY . .

ENV GO111MODULE=on
ENV GOPROXY=https://goproxy.cn,direct

RUN go mod download

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o vessel-service

############################

FROM alpine:latest

RUN mkdir /app

WORKDIR /app

COPY --from=builder /app/vessel-service .

CMD ["./vessel-service"]
