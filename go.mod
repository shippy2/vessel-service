module vessel-service

go 1.13

require (
	github.com/golang/protobuf v1.3.2
	github.com/micro/go-micro v1.18.0
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)
